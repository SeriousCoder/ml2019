import numpy as np
import pandas as pd

import os.path
from os import path

from scipy.sparse import coo_matrix, csr_matrix, lil_matrix, find
from tqdm import tqdm

def sparse_memory_usage(mat):
    try:
        return mat.data.nbytes + mat.indptr.nbytes + mat.indices.nbytes / 1024
    except AttributeError:
        return -1

#handmade subtract
def sub_sparse_to_1d_array(sparse, array):
    arr_rows = array.shape[0]
    arr_cols = array.shape[1]

    indices = sparse.indices
    indptr = sparse.indptr
    data = sparse.data.copy()

    #print(type(data))

    sparse_rows = indptr.size - 1
    sparse_cols = sparse.shape[1]

    if (arr_rows != sparse_rows):
        raise ValueError("Arrays have incompatible sizes")

    for i in range(arr_rows):
        start_ptr = indptr[i]
        end_ptr = indptr[i + 1]

        for index in range(start_ptr, end_ptr):
            data[index] -= array[i]

    return csr_matrix((data, indices, indptr))

def update_r_matrix(A, S):
    indices = S.indices
    indptr = S.indptr
    s_data = S.data.copy()

    fmaxes = np.zeros(S.shape[0], dtype=float)
    smaxes = np.zeros(S.shape[0], dtype=float)

    #Pre-calc maxes
    for i in range(S.shape[0]):
        start_ptr = indptr[i]
        end_ptr = indptr[i + 1]

        for index in range(start_ptr, end_ptr):
            j = indices[index]

            value = S[i, j] + A[i, j]

            if (value > fmaxes[i]):
                smaxes[i] = fmaxes[i]
                fmaxes[i] = value
            elif (value > smaxes[i]):
                smaxes[i] = value

    for i in range(S.shape[0]):
        start_ptr = indptr[i]
        end_ptr = indptr[i + 1]

        for index in range(start_ptr, end_ptr):
            j = indices[index]

            value = S[i, j] + A[i, j]

            if (value < fmaxes[i]):
                s_data[index] -= fmaxes[i]
            else:
                s_data[index] -= smaxes[i]

    return csr_matrix((s_data, indices, indptr))

def update_a_matrix(r):
    indices = r.indices
    indptr = r.indptr
    data = r.data
    #print('r data type: ' + str(type(data[0])))
    a_data = data.copy()

    maxes = np.zeros(r.shape[0], dtype=float)

    for i in range(r.shape[0]):
        start_ptr = indptr[i]
        end_ptr = indptr[i + 1]

        for index in range(start_ptr, end_ptr):
            j = indices[index]
            maxes[j] += max(0., data[index])

    for i in range(r.shape[0]):
        start_ptr = indptr[i]
        end_ptr = indptr[i + 1]

        for index in range(start_ptr, end_ptr):
            j = indices[index]
            if (j == i):
                continue
            a_data[index] = min(0., r[j, j] + maxes[j] - max(0, r[i, j]) - max(0, r[j, j]))

    return csr_matrix((a_data, indices, indptr))

def update_a_matrix_diag(A, R):
    indices = A.indices
    indptr = A.indptr
    data = A.data

    maxes = np.zeros(A.shape[0], dtype=float)

    for i in range(A.shape[0]):
        start_ptr = indptr[i]
        end_ptr = indptr[i + 1]

        for index in range(start_ptr, end_ptr):
            j = indices[index]
            if (j == i):
                continue
            maxes[j] += max(0., R[i, j])

    for i in range(A.shape[0]):
        start_ptr = indptr[i]
        end_ptr = indptr[i + 1]

        for index in range(start_ptr, end_ptr):
            j = indices[index]
            if (i != j):
                continue
            data[index] = maxes[j]

    #return csr_matrix((data, indices, indptr))

def get_klasters(r, a, mvalues, ind_values):
    indices = r.indices
    indptr = r.indptr

    for i in range(r.shape[0]):
        start_ptr = indptr[i]
        end_ptr = indptr[i + 1]

        for index in range(start_ptr, end_ptr):
            j = indices[index]

            value = r[i, j] + a[i, j]


            if (value > mvalues[i]):
                mvalues[i] = value
                ind_values[i] = j
        #print(str(i) + ':\t' + str(mvalues[i]))
    #return (mvalues, ind_values)

def save_result(mv, iv):
    f = open("res.txt", "w")
    #f.write('idx\tksr\tval\n')
    #print(mv[0])
    for i in range(mv.shape[0]):
        s = f'{i}\t{int(iv[i])}\t{mv[i]}'
        print(s)
        f.write(s+'\n')
    f.close()



#Чтение из txt, формирование матрицы
a, b = np.loadtxt('./Gowalla_edges.txt', unpack=True, dtype={'names': ('a', 'b'), 'formats': ('<i8', '<i8')})

S = csr_matrix((np.ones(a.shape,float),(a,b)))

#print(S[:10, :10].A)

S.setdiag(-2.0)
A = csr_matrix((np.zeros(a.shape,float),(a,b)))
R = csr_matrix((np.zeros(a.shape,float),(a,b)))
#tmp = coo_matrix((np.zeros(a.shape,float),(a,b)))

del a
del b

#Алгоритм AP
max_iter = 6

ind = np.arange(S.shape[0])

for i in tqdm(range(max_iter)):
    print('Iter #' + str(i))

    #Re-calculate R
    print('Re-calculate R')
    tmp = S + A
    I = np.ravel(np.argmax(tmp, axis=1))
    #Y = tmp.tocsr()[ind,:].tocsc()[:, I]
    Y = tmp[ind, I]
    tmp[ind, I] = -np.inf
    Y2 = np.max(tmp, axis=1)
    tmp = sub_sparse_to_1d_array(S, Y.T)
    R[ind, I] = S[ind, I] - Y2.T
    R = tmp
    #R = update_r_matrix(A, S)

    print('R is:')
    print(R)
    
    #Re-calculate A (without diag)
    print('Re-calculate A (without diag)')
    A = update_a_matrix(R)

    print('A(-diag) is:')
    print(A)

       #Re-calculate A diag
    print('Re-calculate A diag')
    update_a_matrix_diag(A, R)

    print('A(+diag) is:')
    print(A)

mvalues = np.full(R.shape[0], -np.inf)
ind_values = np.zeros(R.shape[0])
get_klasters(R, A, mvalues, ind_values)
save_result(mvalues, ind_values)

