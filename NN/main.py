from Core import Sequential
from Core import Flatten, Dense, Sigmoid, Dense, SoftMax, ReLU, LeakyReLU, SoftPlus, Dropout

import matplotlib.pyplot as plt
import numpy as np

train_dataset_path = "./mnist_train.csv"
test_dataset_path = "./mnist_test.csv"

train = np.genfromtxt(train_dataset_path, delimiter=",")
x_train = train[:, 1:]
y_train = train[:, 0]

print("Train dataset loaded")

test = np.genfromtxt(test_dataset_path, delimiter=",")
x_test = test[:, 1:]
y_test = test[:, 0]

print("Test dataset loaded")

x_train = x_train.reshape(x_train.shape[0],-1) / 255.0
x_test = x_test.reshape(x_test.shape[0],-1) / 255.0
y_train = y_train.reshape(-1, 1)
y_test = y_test.reshape(-1, 1)

#Прогоняем
NN = Sequential()
NN.add(Flatten())
NN.add(Dense(128))
NN.add(ReLU())
NN.add(Dense(10))
NN.add(SoftMax())

print("Start train")
NN.fit(x_train, y_train)

print("Test NN")
NN.getRes(x_test, y_test)

plt.subplot(2, 1, 1)
plt.plot(range(16), NN.history['accuracy'])
plt.ylabel('Accuracy')
plt.subplot(2, 1, 2)
plt.plot(range(16), NN.history['loss'])
plt.ylabel('Loss')
plt.savefig('plot.png')


#print("accuracy: " + str(NN.history['accuracy']))
#print("loss: " + str(NN.history['loss']))