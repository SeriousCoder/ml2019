import numpy as np
from tqdm import tqdm

from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import accuracy_score, log_loss

#Инициализация
class Layer: pass


class Layer:
    def __init__(self): 
        self.z = None
        self.dZ = None
        
    def forwardPass(self, X): pass  
    
    def backProp(self, dZ, prev_layer = None): pass
    
    def update(self, lr): pass

#Активация
class ReLU(Layer):    
    def forwardPass(self, X):
        self.z = np.where(X > 0., X, 0.)
        return self.z    
    
    def backProp(self, dZ = None, prev_layer = None):  
        self.dZ = dZ
        return self.dZ * np.maximum(prev_layer.z , 0) / prev_layer.z
    
    
class LeakyReLU(Layer):
    def __init__(self, k=0.01):
        self.k = k
        
    def forwardPass(self, X):          
        self.z = np.multiply(X, np.where(X < 0, self.k, 1)) 
        return  self.z    
    
    def backProp(self, dZ = None, prev_layer = None):
        self.dZ = dZ        
        return self.dZ*np.where(prev_layer.z < 0, self.k, 1)
    
    
class SoftPlus(Layer):   
    def forwardPass(self, X):
        self.z = np.log(1 + np.exp(X))
        return self.z  
    
    def backProp(self, dZ = None, prev_layer = None):
        self.dZ = dZ
        return self.dZ * 1/(1+np.exp(-prev_layer.z))
    

class Sigmoid(Layer):
    def forwardPass(self, X):
        self.z = 1/(1+np.exp(-X))
        return self.z
    
    def backProp(self, dZ = None, prev_layer = None):
        self.dZ = dZ
        return self.dZ * self.forwardPass(prev_layer.z) * self.forwardPass(1-prev_layer.z)
    

def softmax(X, theta = 1.0, axis = None):
    
    # make X at least 2d
    y = np.atleast_2d(X)

    # find axis
    if axis is None:
        axis = next(j[0] for j in enumerate(y.shape) if j[1] > 1)

    # multiply y against the theta parameter, 
    y = y * float(theta)

    # subtract the max for numerical stability
    y = y - np.expand_dims(np.max(y, axis = axis), axis)
    
    # exponentiate y
    y = np.exp(y)

    # take the sum along the specified axis
    ax_sum = np.expand_dims(np.sum(y, axis = axis), axis)

    # finally: divide elementwise
    p = y / ax_sum

    # flatten if X was 1D
    if len(X.shape) == 1: p = p.flatten()

    return p
    
                 
class SoftMax(Layer):
    def forwardPass(self, X):
        self.z = softmax(X, axis=1)
        return self.z
    
    def backProp(self, dZ, prev_layer = None):
        return self.z - dZ

#Слои
class Dense(Layer):
    def __init__(self, layer_quantity, activation=None):
        self.W = None
        self.b = None
        self.dW = None
        self.db = None
        self.l_quant = layer_quantity
        self.disp = 1/np.sqrt(layer_quantity)  
        
    def forwardPass(self, X):
        if self.W is None:
            self.W = np.random.uniform(-self.disp, self.disp, size = (X.shape[1], self.l_quant))
            self.b = np.random.uniform(-self.disp, self.disp, size = (1, self.l_quant))
        self.z = X@self.W + self.b
        return self.z    
    
    def backProp(self, dz, prev_layer = None):
        self.dz = dz
        self.db = self.dz.sum(0) / self.z.shape[0]
        self.dW = prev_layer.z.T@self.dz / self.z.shape[0]
        return self.dz @ self.W.T    
    
    def update(self, lr):
        self.W = self.W - self.dW * lr
        self.b = self.b - self.db * lr     
    

class Flatten(Layer):
    def forwardPass(self, X):
        self.z = X
        return self.z    


class Dropout(Layer):
    def __init__(self, prob_rate = 0.5):
        self.z = None
        self.mask = None
        self.rate = prob_rate 
        
    def forwardPass(self, X):
        self.z = X
        self.mask = np.random.choice([0, 1], size=X.shape, p=[self.rate, 1-self.rate])
        return X * self.mask 
    
    def backProp(self, dz, prev_layer = None):
        return dz * self.mask    
    
    def update(self, lr): pass
	
	
#Нейросеть
def crossEntropyLoss(y_real, y_pred):
    return -np.sum(np.log(y_pred) * y_real)

def ProprocessigY(y):
    new_y = np.zeros((len(y), 1))
    for i in range(len(y)):
        new_y[i, 0] = int(y[i])
    return new_y

class Sequential:
    def __init__(self, epochs=16, batch_size=128, lr=0.1, lr_dec=0.01, epoch_decrease=4):
        self.epochs = epochs              
        self.batch_size = batch_size            
        self.lr = lr                      
        self.lr_decrease = lr_dec    
        self.epochs_decrease = epoch_decrease    
        self.layers = list()             
        self._encoder = OneHotEncoder(sparse=False,categories='auto') 
        self.history = {'loss': list(), 'accuracy': list()}
                        
    def add(self, layer): 
        self.layers.append(layer)
    
    def _forwardPass(self, X):
        prev_output = X
        for layer_ind in self.layers:
            prev_output = layer_ind.forwardPass(prev_output)
        return prev_output
    
    def _backProp(self, X, y):
        dZ = y
        for index in range(len(self.layers)-1,0,-1):
            dZ = self.layers[index].backProp(dZ, prev_layer=self.layers[index-1])    
    
    def stepDecrease(self, init_lr, epoch):
        drop = self.lr_decrease
        lr = init_lr * np.power(drop,np.floor((1+epoch)/self.epochs_decrease))
        return lr
    
    def _update(self, epoch):
        for layer_ind in self.layers:
            layer_ind.update(self.stepDecrease(self.lr, epoch))
            
    def predict(self, X):
        prediction = self._forwardPass(X)
        return np.argmax(prediction, axis=1)
    
    def evalute(self, X, Y):
        return(accuracy_score(self.predict(X),Y))
    
    def getRes(self, x_train, x_test):
        print("TRAIN loss :", self.history['loss'][len(self.history['loss'])-1::])
        print("TRAIN accuracy:", self.history['accuracy'][len(self.history['accuracy'])-1::])
        print("TEST accuracy:", self.evalute(x_train, x_test))
        
    def fit(self, X, y=None):
        y_old = np.copy(y)
        y = ProprocessigY(y)
        y = self._encoder.fit_transform(y)
        for epoch in tqdm(range(self.epochs)):
            for ind in range(len(X) // self.batch_size):
                self._forwardPass(X[ind * self.batch_size:(ind+1) * self.batch_size])
                self._backProp(X[ind * self.batch_size:(ind+1) * self.batch_size], y[ind * self.batch_size:(ind+1) * self.batch_size])
                self._update(epoch)
            
            self.history['accuracy'].append(accuracy_score(y_old, self.predict(X)))
            self.history['loss'].append(log_loss(y, self._forwardPass(X)))