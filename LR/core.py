import csv
import pandas as pd
import numpy as np
import datetime
import time
import random
from tqdm import tqdm
import subprocess as sp

sp.call('cls',shell=True)

base_alpha = 0.1
alpha = 0.1
epsilon = 0.00001
max_epochs = 10000
fold_count = 5

batch_size = 256
max_iter = 100

#Нормализация фичи
def normalize(x):
    return (x - x.mean())/x.std()

#Средне квадратическая ошибка 
def R2(x, y):
    return 1 - np.sum(np.power(y - x, 2)) / np.sum(np.power(y - y.mean(), 2))

#Среднеквадратичное отклонение
def RMSE(x, y):
    return np.sqrt(np.sum(np.power(y - x, 2)) / y.shape[0])
	
#Гардиентный спуск
def gradient_descent(fold):
	a = np.ones(fold.shape[1] - 1)
	old_a = 100*a
	N = fold.shape[0]
	epochs = 0
	
	#pbar = tqdm(total=max_iter)
	
	for ep in tqdm(range(max_iter)):

		fold = fold.sample(frac=1)
		y = fold['Target']
		x = fold.drop('Target', axis=1)
		
		for i in range(0, N, batch_size):
			x_mini = x[i : i + batch_size]
			y_mini = y[i : i + batch_size]
		
			while np.linalg.norm(a-old_a) >= epsilon:
				old_a = a

				pred_y = x_mini.dot(a)

				da = (2 / batch_size) * (x_mini.T.dot(pred_y - y_mini))
				a = a - alpha*da

				#if epochs % 500 == 0: print('Passed ' + str(epochs) + ' epochs')
				if epochs > max_epochs:
					#print('Descent finished')
					break
				epochs += 1

	#print('Descent finished')
	#pbar.close()
	return a

#Заголовки фич
head = ['Page Popularity','Page Checkins','Page talking about','Page Category'] \
   + ['Derived_feature_{}'.format(i - 4) for i in range(4, 29)] \
   + ['CC1','CC2','CC3','CC4','CC5','Base Time','Post Length','Post Share Count','Post Promotion Status','H Local'] \
   + ['published_weekday_{}'.format(i - 39) for i in range(39, 46)] \
   + ['base_weekday_{}'.format(i-45) for i in range(45, 52)] \
   + ['Target']
   
csv = pd.read_csv('./Dataset/Training/Features_Variant_1.csv', sep=',', names=head)
#Содержит одни нули - убираем фичу
csv.drop('Post Promotion Status', axis=1, inplace=True)

#Производим нормирование
dataset = csv.drop('Target', axis=1).apply(normalize, axis=0)

#Добавляем константу и добавляем столбец с эталонными значениями
dataset['a0_const'] = 1
dataset['Target'] = csv['Target']

#Перемешаем датасет
dataset = dataset.sample(frac=1)

#Определяем размер фолда
fold_size = round(dataset.shape[0] / fold_count)


alpha = base_alpha
#Делаю несколько прогонов с разными альфами
for e in range(5):
	#Инициируем переменные под результаты
	a_per_fold = pd.DataFrame()
	RMSE_test = []
	RMSE_train = []
	R2_test = []
	R2_train = []

	alpha = alpha / 10
	print('Process train with alpha = {0}'.format(alpha))
	
	start_time = time.time()

	for i in range(fold_count):
		print('Fold #' + str(i))
		#
		test = dataset[i * fold_size:(i + 1) * fold_size]
		
		#
		if i == 0:
			train = dataset[(i + 1) * fold_size:]
		else:
			train = dataset[:i * fold_size]
			if i != 4:
				train = train.append(dataset[(i + 1) * fold_size:], ignore_index=False)
		
		#
		a = gradient_descent(train)
		a_per_fold = a_per_fold.append(a, ignore_index=True)
		
		#
		train_pred = train.drop('Target', axis=1).dot(a)
		R2_train.append(R2(train_pred, train['Target']))
		RMSE_train.append(RMSE(train_pred, train['Target']))
		
		#
		test_pred = test.drop('Target', axis=1).dot(a)
		R2_test.append(R2(test_pred, test['Target']))
		RMSE_test.append(RMSE(test_pred, test['Target']))

	res = pd.DataFrame(np.vstack([R2_test, R2_train, RMSE_test, RMSE_train]), index=['R2_test', 'R2_train', 'RMSE_test', 'RMSE_train'])
	print(a_per_fold)
	res = res.append(a_per_fold.T)
	res.columns = ['T' + str(i + 1) for i in range(fold_count)]
	res = pd.concat([res, res.mean(axis=1).rename('E'), res.std(axis=1).rename('STD')], axis=1)

	end_time = time.time() - start_time
	
	res.to_csv('./LR_(epochs{0})(aph{1})(eps{2})(time{3})_{4}.csv'.format(max_epochs, alpha, epsilon, round(end_time), datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")))
	
	#res.to_excel('./LR_(epochs{0})(aph{1})(eps{2})(time{3})_{4}.xlsx'.format(max_epochs, alpha, epsilon, round(end_time), datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")), columns=res.columns)
	print('Elapsed time: ' + str(end_time))