import pandas as pd
import numpy as np

from tqdm import tqdm
import time

from scipy.sparse import csr_matrix, lil_matrix, vstack
from sklearn.datasets import make_regression

from defs import Func
from data_loader import data_load

###########################################################

#X = data_load()
#y = pd.read_csv('score.csv', names=['Score'])
#y = y['Score'].values
#print(len(y))
#print(y[0])

X, y, coef = make_regression(n_samples=10000, n_features=8, n_targets=1, n_informative=4, coef=True)
X = csr_matrix(X)

#print(sm.shape)
#print(scores)

folds_index = 5
fold_size = round(X.shape[0] / folds_index)
RMSE_test = []
RMSE_train = []

start_time = time.time()

for i in range(folds_index):
    print("i = " + str(i))
    test = X[i * fold_size:(i + 1) * fold_size]
    testT = y[i * fold_size:(i + 1) * fold_size]
    if i == 0:
        train = X[(i + 1) * fold_size:, :]
        trainT = y[(i + 1) * fold_size:]
    else:
        train = X[:i * fold_size, :]
        trainT = y[:i * fold_size]
        if i != 4:
            #print(train.shape)
            #print(X[(i + 1) * fold_size:, :].shape)
            train = vstack((train, X[(i + 1) * fold_size:, :]))
            trainT = np.concatenate((trainT, y[(i + 1) * fold_size:]))
    
    Target = trainT
    Features = train
    w0, w1, V = Func.sgdFFM(Features, Target, 1e4, 1e-3)

    
    train_pred = Func.predict(Features, V, w0, w1)
    
    RMSE_train.append(Func.RMSE(train_pred, Target))
    
    TargetT = testT
    FeaturesT = test
    test_pred = Func.predict(FeaturesT, V, w0, w1)
    RMSE_test.append(Func.RMSE(test_pred, TargetT))
    
    del test
    del testT
    del train
    del trainT

df = pd.DataFrame(np.vstack([RMSE_train, RMSE_test]), 
                  index=['rmse_train','rmse_test'])

df = pd.concat([df, df.mean(axis=1).rename('mean'),
                df.std(axis=1).rename('std')], axis=1)

print(df)

end_time = time.time() - start_time
print('Elapsed time: ' + str(end_time))

