import pandas as pd
import re
import os
import sys

import joblib
from tqdm import tqdm
from io import StringIO
from scipy.sparse import csr_matrix, lil_matrix

def data_load():
	#читаем датасет c фичами фильмов
	df_f = pd.read_csv('./movie_titles.csv', names=['MovieID', 'Year', 'Title'], encoding = "ISO-8859-1")
	df_f = df_f.set_index('MovieID')

	print("INFO: movie_titles.csv loaded")

	if os.path.isfile('sparse_df.bin'):
		sm = joblib.load('sparse_df.bin')
	else:

		###читаем датасет с инфой о пользователях
		f = open('./combined_data_1.txt')
		file = f.read()

		movieID_u = list(map(lambda x: int(x[:-1]), re.findall(r'\d+\:', file)))
		df_u = re.split(r'\d+\:', file)
		df_u = df_u[1:]    

		df = []
		for df_i, movieID_i in tqdm(zip(df_u, movieID_u)):
			sub_df = pd.read_csv(StringIO(df_i), names=['UserID', 'Score', 'Date'])
			sub_df['MovieID'] = movieID_i
			df.append(sub_df)


		df = pd.concat(df)
		df_f = df_f.drop(df_f[df_f.index>df.MovieID.unique().shape[0]].index)
		print("INFO: user scores loaded")

		u_size = df.UserID.unique().shape[0]
		m_size = df.MovieID.unique().shape[0]

		df.sort_values(by=['UserID', 'MovieID'], inplace=True)
		df.reset_index(drop=True, inplace=True)
		df_targ = df.Score
		df_targ.to_csv('score.csv', index=False, header=False)

		# разреженная матрица
		sm = lil_matrix((df.shape[0], u_size + m_size))
		#print(sm.shape)

		j=0
		temp = df.UserID.iat[0]

		for i in tqdm(range(df.shape[0])):
		
			if(temp<df.UserID.iat[i]):
			    j+=1
			    temp = df.UserID.iat[i]
			    
			sm[i, j] = 1
			sm[i, u_size + df.MovieID.iat[i]-1] = 1

		print(sm[0,0])

		sm = csr_matrix(sm)
		joblib.dump(sm, 'sparse_df.bin')

		del df
		del df_u
		del df_f
		del movieID_u
		del df_targ
	return sm

