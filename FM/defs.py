import numpy as np
from tqdm import tqdm

class Func:
	@staticmethod
	def RMSE(x, y):
		return np.sqrt(np.sum(np.power(y - x, 2)) / y.shape[0])

	@staticmethod
	def narrTheta(lr, iter_num):
		return lr / np.sqrt(iter_num)

	@staticmethod
	def sgdFFM(X, y, max_iter=1e3, step=1e-3, epsilon=1e-6, batch_size=256, k=9):
		w0 = 0
		iter_num = 1
		N = X.shape[0]
		np.random.seed(42)
		w1 = np.random.normal(size=X.shape[1])
		V = np.random.normal(size=(X.shape[1], k))
		#V = np.zeros((X.shape[1], k))

		#pbar = tqdm(total=max_iter, initial=iter_num)
		for iter_num in tqdm(range(0,int(max_iter))):
			#print(str(iter_num) + " (max " + str(max_iter) + ")")

			for i in range(0, N, batch_size):

				tX = X[i: i + batch_size, :]
				tY = y[i: i + batch_size]
				#print(random_batch)
				new_y = Func.predict(tX, V, w0, w1)
				# print(X[random_batch].shape)

				dy = 2 * (new_y - tY)
				# print(iter_num, dy)
				w0 -= dy.mean() * step
				w1 -= tX.T @ dy * step / batch_size
				for f in range(k):
					dA = (tX.dot(V[:,f]))
					#print(tX.shape)
					#print(V[:,f].shape)
					dA = dA.reshape(-1, 1)
					dA = tX.multiply(dA)
					#print(dA.shape)
					#256*8x8*1 = 256*8
					dB = (tX.power(2)).multiply(V[:,f])
					#print(dA.shape)
					#print(dB.shape)
					#print(dy.shape)
					foo = dy.reshape(-1, 1)*(dA-dB).A
					V[:,f] -= step*np.asarray(foo.mean(axis=0).T)

				if (np.linalg.norm(tY - new_y) < epsilon):
					print(np.linalg.norm(tY - new_y))
					return w0, w1, V

				if (iter_num%100==0):
					print(np.linalg.norm(tY - new_y))

			#iter_num += 1
		return w0, w1, V

	@staticmethod
	def nonLin(X, V):
		A = (X@V)**2
		B = (X.power(2))@(V**2)
		return 1/2*((A-B)@np.ones(V.shape[1]))

	@staticmethod
	def predict(X, V, w0, w1):
		return w0 + X @ w1 + Func.nonLin(X, V)
